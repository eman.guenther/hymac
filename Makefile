CC = gcc
CFLAGS = -Wall -ggdb -Wno-unused-value

all: hymac doc

hymac: driver init lexer parser parselist generate_mc cf_graph generate_c stack
	$(CC) $(CFLAGS) -o bin/hymac bin/driver.o bin/init.o bin/lexer.o bin/parser.o bin/parselist.o bin/code_gen_mc.o bin/controll_flow_graph.o bin/code_gen_c.o bin/stack.o

driver: src/driver.c
	$(CC) $(CFLAGS) -c $< -o bin/driver.o

init: src/init.c
	$(CC) $(CFLAGS) -c $< -o bin/init.o

lexer: src/lexer.c
	$(CC) $(CFLAGS) -c $< -o bin/lexer.o

parser: src/parser.c
	$(CC) $(CFLAGS) -c $< -o bin/parser.o

parselist: src/parselist.c
	$(CC) $(CFLAGS) -c $< -o bin/parselist.o

generate_mc: src/code_gen_mc.c
	$(CC) $(CFLAGS) -c $< -o bin/code_gen_mc.o

cf_graph: src/controll_flow_graph.c
	$(CC) $(CFLAGS) -c $< -o bin/controll_flow_graph.o

generate_c: src/code_gen_c.c
	$(CC) $(CFLAGS) -c $< -o bin/code_gen_c.o

stack: src/stack.c
	$(CC) $(CFLAGS) -c $< -o bin/stack.o

.PHONY: doc
doc: slides api

slides:
	make slides -C doc

api:
	make api -C doc

.PHONY: clean
clean:
	rm bin/.* 2> /dev/null
	make clean -C doc/

