# Software hymac

This software called **hymac** is a compiler for the assembly language of the hypothetical machine used in the course I-110 (Grundlagen der Informatik I) at the *Hochschule für Technik und Wirtschaft Dresden (HTW Dresden)*.

The hypothetical machine was developed by [Prof. Sabine Kühn](https://www2.htw-dresden.de/~skuehn/) who is also the lecturer of the course I-110 (Grundlagen der Informatik I).

The software compiles the assembly-like language into machine code or into a representation of this code in the language C.

## Dependencies

* make >= 4.1
* doxygen >= 1.8.13
* pandoc >= 2.7.3

## Build

To build the software run `$ make` in the root directory of this repository.

For the documentation the software `pandoc` has to be downloaded and placed in the `extern` directory in a folder named `pandoc`. After that the documentation can be generated with `$ make doc`.

## Usage

* `$ hymac [options] file`
* filetype has to be `.hma.txt`
* `-c <code>` destination language with *mc* (machine code) or *c* as options (default is both)
* `-s <address>` specifying the starting address (default is 0x0)
* `-o <file>` specifies the output file
* `--help` and `--version` for general information

## Documentation

The documentation for this project can be found in the folder named `doc`.

There is an API documentation which explains the functionality of the single compiler modules in detail.

There are also slides which can be used to present the project in a lecture or at similar events. They contain more context information, especially facts about the process of origination, reasons for design decitions and more.

## License
The software and documentation is licensed under the Apache License Version 2.0. For further information see the files called `NOTICE.txt` and `LICENSE.txt`.

The logo of the HTW Dresden is in the public domain.

reference: [HTW-Logo at Wikipedia](https://de.wikipedia.org/wiki/Datei:Htw-dresden-logo.svg)

This picture just contains simple geometric figures. They does not exceed the [threshold of originality](https://en.wikipedia.org/wiki/Threshold_of_originality) and therefore does not need copy right. The picture belongs to the public domain. But although it does not have copy right protection it may be have other restrictions. For more information see [WP:PD#Fonts](https://en.wikipedia.org/wiki/Wikipedia:Public_domain#Fonts) or [Template talk:PD-textlogo](https://commons.wikimedia.org/wiki/Template_talk:PD-textlogo).

## Further information

To get more information about compiler design and decompilation techniques I can recommend the following literature:

* [Dick Grune et al.: Modern Compiler Design (2012)](https://link.springer.com/book/10.1007%2F978-1-4614-4699-6)
* [Cristina Cifuentes: Reverse Compilation Techniques (1994)](http://www.phatcode.net/res/228/files/decompilation_thesis.pdf)
