---
title: hymac
subtitle: hypothetic machine compiler
author: Emanuel Günther
institute: HTW Dresden
date: \today
lang: en-GB

colorlinks: true
linkcolor: black
urlcolor: htwddblue
section-titles: false

theme: hymac
...

# PREFACE

## Interpreter

* first an interpreter for the hypothetic machine was created
	* [interpreter](src/interpreter.md)
* founded by an interest in compiler and interpreter construction and the job as tutor
* audience are first semester students in the study course of media informatics
* hyMaIntp: [`https://gitlab.com/eman.guenther/hyMaIntp`](https://gitlab.com/eman.guenther/hyMaIntp)

# COMPILER STRUCTURE

## Overview

::: columns

:::: column

* **lexer**: reads source code word by word, writes it into tokens
* **parser**: takes tokens and checks the syntax, creates representation to work with
* **code generator**: generates destination source code  
  **or interpreter**: runs the program in an interactive mode

::::

:::: column

![flowchart compiler](res/flowchart_compiler.png){ height=65% }

::::

:::

## Language

* derived from assembly language
* just simple instructions (one address)
* accumulator stores intermediate results
* [grammar](src/grammar.md)
* [specification](src/specification_hyma.md)

# LEXER

## Functionallity

* **token:** element which is part of a grammar
	* e.g. natural language: one word is one token
* lexer "reads in" tokens one by one
* information is stored in variable(s)
* parser takes this tokens and use them

## Functions and Variables

* function `start_lexer`:
	* reads code from file
	* creates one input "string" (simpler memory management)
* token struct
	* type of token (EOF, OPCODE, etc.)
	* pointer to location in input string
	* length of the token (measured in char)
	* line number in which the token is located $\rightarrow$ error management
* one global token variable which is overwritten every time a new token is lexed ($\rightarrow$ memory management)
* function `get_next_token`:
	* lexes the next token and writes information to token variable

## Implementation of `get_next_token`
* some macro functions for fast recognition of characters
	* upper case & lower case letters, digits, layout, brackets, etc.
	* e.g.  
	  `#define is_digit(ch) ('0' <= (ch) && (ch) <= '9')`
* layout and C-style comments (`//`) are skipped
* type of token in most cases is decided by first charakter
	* if identified: token is recognized, information is stored
	* special case *`'L'`*:
		* `LOAD` or `L1` (label) are possible
		* decision on second charakter

## Example "`LOAD n`"

"`LOAD`" token:

> type: OPCODE  
> pt: 0x...  
> len: 4  
> line\_number: 1  

"`n`" token:

> type: VARNAME  
> pt: 0x...  
> len: 1  
> line\_number: 1  

"`\0`" token:

> type: END\_OF\_LINE  
> pt: 0x...  
> len: 1  
> line\_number: 1  

# PARSER

## Functionallity

* recognizes structure of the code and represents it in memeory
* for higher programming languages representation is *abstract syntax tree*
* hypothetic machine has an assembly language like language, which has a linear structure
	* in most cases simpler to handle $\rightarrow$ parselist
* parser in general does
	* get a token from the lexer
	* check for the right syntax/order
	* store the information in a variable
	* connect this information with existing representation
* when the EOF-token is parsed the structure representing the code is complete (if no errors occur)

## Variables and Definitions

* `enum type_of_data`: type of data stored in singly linked list
* struct listelem: (singly linked list)
	* tod (type of data stored in this element)
	* data (pointer to the data)
	* next (pointer to the next element in the list)
* functions for inserting in list, deleting whole list

![listelem](res/listelem.png){ height=30% }

- - -

### parselist

* stuct line:
	* addr (address of the line)
	* lb (pointer to the label of the line, `NULL` if not existing)
	* op\_code (integer representation of operation code)
	* operand (pointer to variable or label - depends on op\_code

* multiple tokens get parsed into one line

![line structure example](res/line_struct.png){ height=30% }

- - -

### labellist

* struct label:
	* name (number of the label, e.g. "L1" $\rightarrow$ "1")
	* addr (destination address of the label)
	* ln (pointer to the line belonging to this label)

![label structure example](res/label_structure.png){ height=35% }

- - -

### variablelist

* struct variable:
	* name (name of the variable as "string")
	* value (initial value of the variable)
	* addr (address of the line of the variable definition)

![variable structure example](res/variable_structure.png){ height=50% }

## Example parselist

![parselist example](res/parselist.png){ height=70% }

## Example "`L1 LOAD n`"

![parse structure example](res/parse_structure_example.png){ height=80% }

## Charakteristics of this parser

* parser steps:
	* get a token
	* check the order (assembly language code has a simple syntax)  
		separate checks for cases before and after STOP
	* store the information
	* with END\_OF\_LINE or EoF the information collected is validated  
	  $\rightarrow$ creates an error if information is missing  
		$\rightarrow$ otherwise the struct is inserted in the list
	* after parsing all tokens:
		* validating label and variable lists (checking for missing information)  
		  $\rightarrow$ this can occur if labels or variables are used which are not defined
		* e.g. `JUMP L2` would create an entry in the label list where the destination is missing  
		  this information is inserted when "`L2 <opcode> <operand>`" is hit

## Implementation
* separate label and variable lists for better navigation and structure
	* linking between them is possible
* two different loops for parsing
	* one before STOP (opcode, labels, variables)
	* one after STOP (just variable definitions)
* some topics:
	* allocating listelements
	* error handling
	* transformations (string$\rightarrow$number; string$\rightarrow$opcode)
	* searching variables or labels in their lists

# MACHINE CODE GENERATION

## Algorithm
* simple, because also linear
* go through parselist and variablelist (in this order)
* take the information and write machine code representation to file
* got even simpler for me because the numbers (macros) I used for the operations are
	"`0x100 | <opcode>`"  
	e.g. LOAD is 0x58, so the representation is 0x158
	* just need to filter this out to get the right code
* with variables it is simple as well

# C CODE GENERATION

## Functionallity

* a lot more difficult
* C is a higher programmming language than the hypothetic machine assembly language
* techniques of decompilation used
	* code gets structured in basic blocks
	* basic blocks are linked together to a controll flow graph (cfg)
	* higher level language structures are recognized from the cfg
* code is generated during the recognition of the structures

## Controll Flow Graph

* iterative approach
	* create basic blocks (linear, in equivalence to the parselist)
		* rules: `JUMP | JUMPZERO` just at the end and label definitions just at the begin of a block
	* create linked graph (linear, look for single or 2-way outgoing edges)
		* for `JUMP | JUMPZERO`: search for destination of label (basic block) and create link
* linear links (doubly linked list) remains for simpler search and deleting


## Example: n!, n <= 10

![code flow graph example](res/cfg_example.png){ height=80% }

## Structure Recognition

* variable definitions are simply generated by using the variable list
* recognition of structures with recursive function
* nodes/blocks can be marked
* environment of the node/block determines the behavior and code generated
* detection of loops (while and do-while), conditions and their ends

## Recognized Cases I

![recognized cases unmarked](res/recognized_cases_unmarked.png)

## Recognized Cases II

![recognized cases marked](res/recognized_cases_marked.png)

## Example: Condition

	LOAD n
	QUOT three
	JUMPZERO L1
	LOAD n
	MUL three
	SUB three
	L1 STORE result
	STOP
	5 {n}
	3 {three}
	0 {result}

- - -

![condition example](res/condition_example.png){ height=75% }

- - -

![condition recognition](res/condition_example_00.png){ height=75% }

- - -

![end of condition recognition](res/condition_example_01.png){ height=75% }

- - -

![condition false branch](res/condition_example_02.png){ height=75% }

- - -

![sequence recognition](res/condition_example_03.png){ height=75% }

- - -

![end of condition, code generation](res/condition_example_04.png){ height=75% }

- - -

## Example: Loop

	L1 LOAD n
	ADD result
	STORE result
	LOAD n
	SUB one
	STORE n
	JUMPZERO L2
	JUMP L1
	L2 STOP
	5 {n}
	0 {result}
	1 {one}

- - -

![loop example](res/loop_example.png){ height=75% }

- - -

![loop recognition](res/loop_example_00.png){ height=75% }

- - -

![loop branch](res/loop_example_01.png){ height=75% }

- - -

![end of loop](res/loop_example_02.png){ height=75% }

- - -

![after loop](res/loop_example_03.png){ height=75% }

# LINKS

## Further information (compiler design)

* [Dick Grune et al.: Modern Compiler Design (2012)](https://link.springer.com/book/10.1007%2F978-1-4614-4699-6)
* [Cristina Cifuentes: Reverse Compilation Techniques (1994)](http://www.phatcode.net/res/228/files/decompilation_thesis.pdf)

## Sources and License

GitLab Repository: [`https://gitlab.com/eman.guenther/hymac`](https://gitlab.com/eman.guenther/hymac)

License (code and documentation): Apache License 2.0: [`https://www.apache.org/licenses/LICENSE-2.0`](https://www.apache.org/licenses/LICENSE-2.0)
