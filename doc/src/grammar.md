G = (N, T, S, P) with

N = { prog, code, label, opcode, calcode, varname, jmpcode, number, letter, digit, vardef },

T = { '\n', 'L', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'LOAD', 'STORE', 'ADD', 'SUB', 'MUL', 'MOD', 'QUOT', 'JUMP', 'JUMPZERO' },

S = prog,

P = {
    prog -> code STOP '\n' vardef,
    code -> (label? ' ' opcode '\n')*,
    label -> 'L' number,
    opcode -> (calcode ' ' varname) | (jmpcode ' ' label),
    calcode -> 'LOAD' | 'STORE' | 'ADD' | 'SUB' | 'MUL' | 'MOD' | 'QUOT',
    varname -> letter varname?,
    jmpcode -> 'JUMP' | 'JUMPZERO',
    vardef -> (number '{' varname '}' '\n')*,
    number -> digit number?,
    letter -> 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z',
    digit -> '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
} 
