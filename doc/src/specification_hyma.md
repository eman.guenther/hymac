# SPECIFICATION OF THE ASSEMBLY-LIKE "HYPOTHETISCHE MASCHINE"

## General Information

* 4-byte machine
	* 1 for opcode
	* 3 for address or value
* uses unsigned int values
* simple instruction set (see below)
* every loaded or calculated value is stored in the accumulator (central register)
* C-style comments ("`//`") are allowed

## Structure of an Instruction

	<instruction>: (<label>) <operation> <operand>
	<operand>: <label> | <variable_name>

The precise operand depends on the operation in front of it. Jumps (JUMP and JUMPZERO) require a label as operand while all other (calculating) operations need a variable name.

## Instruction Set of the Hypothetical Machine

| Code | Operation | Operand | Explanation                                                         |
|-----:|:----------|---------|:--------------------------------------------------------------------|
|   58 | LOAD      | varname | loads a variable into the accumulator                               |
|   50 | STORE     | varname | stores the value of the accumulator in a variable                   |
|   5A | ADD       | varname | adds the value of a variable to the accumulator                     |
|   5B | SUB       | varname | subtracts the value of a variable from the accumulator              |
|   5C | MUL       | varname | multiplies the value of a variable with the accumulator             |
|   5D | QUOT      | varname | divides the accumulator by the value of a variable                  |
|   5E | MODULO    | varname | executes accumulator modulo variable                                |
|   47 | JUMP      | label   | jumps to a specified label                                          |
|   48 | JUMPZERO  | label   | jumps to a specified label, if the value of the accumulator is null |
|   45 | STOP      | none    | stops the execution of the program                                  |

## Variable Definitions

Variables are defined in the following way:
	<value> {<variable_name>}
Variable definitions are only allowed after the "`STOP`" operation.


# EXAMPLE PROGRAM
A sample programm for subtracting 1 from 5 in a loop until the result is 0 looks like:	
L2  LOAD five
    JUMPZERO L1
    SUB one
    STORE five
    JUMP L2
L1  STOP
   5 {five}
   1 {one}

