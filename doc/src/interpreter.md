# INTERPRETER `hyMaIntp`

The interpreter `hyMaIntp` was the first project dealing with the handling of the assembly-like language of the hypothetical machine. It's source code can be found at [gitlab](https://gitlab.com/eman.guenther/hyMaIntp).

## Brief overview about the functionality

* parameter check
	* check for correct usage
	* filename and options have to be valid
* lexer
	* chars are read from the file one by one
	* tokens are created from multiple chars
* parser
	* check for the right order of the tokens
	* simple list of tokens is easy to apply and use
* interpreter
	* allocates memory for all variables
	* go through the parser-list
	* after each step the user has the option to see debug infos
* debugger/interpreter
	* program asks for input
	* can print state of variables etc.

* general infos:
	* is written with fixed length arrays
	* max 8 char variable names
	* max L0 to L9 for labels
