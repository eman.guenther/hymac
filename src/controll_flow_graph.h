/** \file controll_flow_graph.h
    \brief implementation of a controll flow graph
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#pragma once

#include "includes.h"
#include "parselist.h"
#include "parser.h"

/** \struct bblock
    \brief data structure representing a basic block of code
    All parsed lines are restructured as so called basic block. A basic
    block consists of a sequence of basic instructions.
*/

typedef struct bblock bblock;
struct bblock {
  int numInEdges;    ///< number of in edges
  bblock **inEdges;  ///< array of pointers to predecessors
  int numOutEdges;   ///< number of out edges
  bblock **outEdges; ///< array of pointers to successors
  int numLines;      ///< number of lines this block contains
  listelem *ln;      ///< first line in this block
  bool marked;       ///< marker for structure recognition in code generation
};

/** \brief header of the controll flow graph
 */
extern bblock *graph;

/** \brief appends a pointer to an existing array
    Reallocates the memory for the array and appends the pointer.
    @param array the existing array
    @param pt the pointer which should be appended
    @param num pointer to the number of pointers stored in the array
    @return returns the new array in case of success, NULL otherwise
*/
extern bblock **append_pt_to_array(bblock **array, bblock *pt, int *num);

/** \brief recognizes a basic block and fills in some information
    Recognizes a basic block. The number of lines and the first line in the
    block is written to bb.
    @param bb basic block in which the information is written
    @param curr_line current line at which the recognition is started
    @returns returns bb in case of success, otherwise NULL
*/
extern bblock *recognize_block(bblock *bb, listelem *curr_line);

/** \brief iterative implementation of cfg generation
    Goes through the parselist and creates basic blocks as a list.
    After that the actual valid links are created in a second loop.
    @param parselist list created from the parser
    @return cfg in case of success, NULL otherwise
*/

/** \brief add connection between two basic blocks
    In the frist given basic block the incoming edges are appended by a link
    to the previous block. In equivalent manner the second given basic block
   will be appended by an outgoing edge to the current basic block.
    @param bb current basic block, ingoing edges are appended
    @param prev previous basic block, outgoing edges are appended
    @return EXIT_SUCCESS in case of success, EXIT_FAILURE otherwise
*/
extern int add_connection(bblock *bb, bblock *prev);

/** \brief search the graph for a basic block containing the searched label
    @param lb label which is searched for
    @return pointer to basic block containing the label, NULL in case of
    failure
*/
extern bblock *search_graph_for_label(label *lb);

/** \brief creates the controll flow graph for a given parselist
    The function will create a controll flow graph. This is be done iterative
    depending on the parselist. Even though the graph will be linked correctly,
    in each case the first incoming and outgoing edges show to the previous
    respectively next basic block in a list based on the order in the parselist.
    @param parselist list created from the parser
    @return Returns the controll flow graph. In case of an error NULL is
    returned.
*/
extern bblock *create_cfg(listelem *parselist);

/** \brief deletes the controll flow graph
    Deletes the complete graph and frees all allocated memory.
*/
extern void delete_graph();
