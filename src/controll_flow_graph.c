/** \file controll_flow_graph.c
    \brief contains the implementation of the controll flow graph
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "controll_flow_graph.h"

bblock *graph = NULL;

bblock **append_pt_to_array(bblock **array, bblock *pt, int *num) {
  bblock **edges = (bblock **)malloc(sizeof(bblock *) *
                                     ((*num) + 1)); // allocating new array
  if (edges == NULL) {
    printf("append_basic_block: error at allocating memory\n");
    return NULL;
  }
  if (*num != 0) {
    for (int i = 0; i < *num; i++) { // copying the old array
      edges[i] = array[i];
    }
    free(array); // free the old array
  }
  edges[*num] = pt; // append the new in edge, num is new_num-1
  array = edges;
  (*num)++; // increase the number of in edges
  return edges;
}

bblock *recognize_block(bblock *bb, listelem *curr_line) {
  listelem *le = curr_line;
  line *ln = (line *)curr_line->data;
  int num = 1;
  while (ln->op_code != JUMP && ln->op_code != JUMPZERO) {
    le = le->next;
    if (le == NULL) {
      break;
    }
    ln = (line *)le->data;
    if (ln->lb != NULL) {
      break;
    }
    num++;
  }
  bb->numLines = num;
  bb->ln = curr_line;
  return bb;
}

int add_connection(bblock *bb, bblock *prev) {
  bb->inEdges = append_pt_to_array(bb->inEdges, prev, &bb->numInEdges);
  if (bb->inEdges == NULL) {
    return EXIT_FAILURE;
  }
  prev->outEdges = append_pt_to_array(prev->outEdges, bb, &prev->numOutEdges);
  if (prev->outEdges == NULL) {
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

bblock *search_graph_for_label(label *lb) {
  bblock *bb = graph;
  if ((label *)((line *)bb->ln->data)->lb == lb) {
    return bb;
  }
  while (bb->outEdges != NULL) {
    bb = bb->outEdges[0];
    if ((label *)((line *)bb->ln->data)->lb == lb) {
      return bb;
    }
  }
  return NULL;
}

bblock *create_cfg(listelem *parselist) {
  listelem *curr_line = parselist;
  bblock *prev = NULL; // previous basic block

  // create basic blocks for whole code
  do {
    bblock *bb = (bblock *)malloc(sizeof(bblock));
    if (bb == NULL) {
      printf("cfg: error at allocating memory\n");
      return NULL;
    }

    bb->numInEdges = 0;
    bb->inEdges = NULL;

    if (graph == NULL) {
      graph = bb;
    } else {
      if (add_connection(bb, prev) == EXIT_FAILURE) {
        return NULL;
      }
    }
    bb->numOutEdges = 0;
    bb->outEdges = NULL;

    bblock *rbb = recognize_block(bb, curr_line);
    if (rbb == NULL) {
      return NULL;
    }

    bb->marked = false;

    prev = bb;
    for (int i = 0; i < bb->numLines;
         i++) { // seek to the first line of the next block
      curr_line = curr_line->next;
    }
  } while (curr_line != NULL);

  // appending the links
  bblock *bbtmp = graph; // temporary basic block

  while (bbtmp->outEdges != NULL) {
    listelem *le = bbtmp->ln;
    for (int i = 1; i < bbtmp->numLines; i++) { // seek to last line in block
      le = le->next;
    }
    line *ln = (line *)le->data;
    bblock *sbb;

    switch (ln->op_code) {
    case JUMP:
    case JUMPZERO:
      sbb = search_graph_for_label((label *)ln->operand);
      if (add_connection(sbb, bbtmp) == EXIT_FAILURE) {
        return NULL;
      }
      if (ln->op_code == JUMP)
        break;
    default:
      if (add_connection(bbtmp->outEdges[0], bbtmp) == EXIT_FAILURE) {
        return NULL;
      }
      break;
    }

    bbtmp = bbtmp->outEdges[0];
  }

  return graph;
}

void delete_graph() {
  bblock *bb = graph;
  bblock *next;
  bool run = true;
  if (bb == NULL)
    return;

  while (run) {
    if (bb->outEdges == NULL)
      run = false;
    else
      next = bb->outEdges[0];
    free(bb->inEdges);
    free(bb->outEdges);
    free(bb);
    if (run)
      bb = next;
  }
  graph = NULL;
}
