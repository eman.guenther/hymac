/** \file init.c
    \brief brief description of init.c
 */
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "init.h"

void print_usage(void) {
  printf("\n");
  printf("Usage: hymac [options] file...\n");
  printf("The input filetype has to be \".hma.txt\".\n\n");
  printf("Options:\n");
  printf("  -c <code>\tCompile code to the specified <code> type. Options ");
  printf("are \"mc\" for machine code  and \"c\" for the programming ");
  printf("language C. Without this option both codes are generated.\n");
  printf("  -s <address>\tCompile code and set the begin to ");
  printf(" <address>. The default address used is 0x0000.\n");
  printf("  -o <file>\tWrite the output into <file>.\n");
  printf("  --help\tDisplay this information.\n");
  printf("  --version\tDisplay program version and copyright information.\n");
}

void print_version(void) {
  // specify version and copyright
  printf("\n");
  printf("hyMaIntp vx.x (compiler for the language of the \"hypothetische ");
  printf("Maschine\", used at the HTW Dresden in modul I-110 by Prof. ");
  printf("Dr.-Ing. S. Kühn)\n");
  printf("Copyright (C) 2019 Emanuel Günther\n");
  printf("\n"); // copyright
}

int check_file(char *filename) {
  char extension[9];
  int len = strlen(filename);

  for (int i = len - 8; i < len;
       i++) { // copying the file extension to var extension
    extension[i - len + 8] = filename[i];
  }
  extension[8] = '\0';

  // check for file type
  if (strcmp(extension, ".hma.txt") != 0) {
    printf("Wrong filetype: %s\n", filename);
    return EXIT_FAILURE;
  }

  // check for existence and readability of the file
  // should work for Linux and Windows with MinGW-gcc
  if (access(filename, R_OK) != -1) {
    return EXIT_SUCCESS; // file can be read
  } else {
    printf(
        "File \"%s\" does not exist or cannot be read. Please state a valid ",
        filename);
    printf("file.\n");
    return EXIT_FAILURE;
  }
}

args check_parameters(int argc, char **argv) {
  char c;
  args a = {-1, ' ', NULL, 0};
  struct option long_opt[] = {{"help", no_argument, NULL, 'h'},
                              {"version", no_argument, NULL, 'v'}};

  unsigned long addr;

  // look through all paramters
  while ((c = getopt_long(argc, argv, "c:ho:s:v", long_opt, NULL)) != -1) {
    switch (c) {
    case 'c': // compile to ...
      if (optarg == NULL) {
        printf("optarg == NULL\n");
        exit(EXIT_FAILURE);
      }

      // argument should be "c" or "mc"
      if (strcmp(optarg, "c") == 0 || strcmp(optarg, "mc") == 0) {
        a.c_opt = optarg[0];
      } else {
        printf("Wrong argument for option -c.\n");
        print_usage();
        exit(EXIT_FAILURE);
      }
      break;
    case 'h':
      print_usage();
      exit(EXIT_SUCCESS);
      break;
    case 'o': // output to ...
      a.o_opt = optarg;
      break;
    case 's': // start with address ...
      if (strtol(optarg, NULL, 16) < 0) {
        printf("Warning: negative starting address is treated as 0\n");
        addr = 0;
      } else {
        addr = strtoul(optarg, NULL, 16);
      }
      if (addr % 4 != 0) {
        printf("Warning: The specified starting address is not a multiple of "
               "4!\n");
      }

      a.s_opt = (unsigned int)addr;
      break;
    case 'v':
      print_version();
      exit(EXIT_SUCCESS);
      break;
    default: // error
      print_usage();
      exit(EXIT_FAILURE);
      break;
    }
  }

  if (optind == argc) {
    printf("Too little arguments.\n");
    print_usage();
    exit(EXIT_FAILURE);
  }

  for (int i = optind; i < argc; i++) {
    if ((i - optind) > 0) {
      printf("Too many arguments.\n");
      print_usage();
      exit(EXIT_FAILURE);
    }
    a.f_opt = i;
    if (check_file(argv[i]) != 0) {
      print_usage();
      exit(EXIT_FAILURE);
    }
  }

  return a;
}
