/** \file code_gen_mc.h
    \brief header file of the code generation of machine code
 */
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#pragma once

#include "includes.h"
#include "parser.h"

/** \brief Generates machine code and writes it to a file.
    This function generates machine code from the parsed code and writes
    it into the specified file.
    @param filename the file in which the generated code is written
    @return filename in case of success, NULL in case of an error
 */
extern char *generate_mc(char *filename);
