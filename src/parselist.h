/** \file parselist.h
    \brief contains all definitons needed for the list in which the code is
    parsed This file contains some additional macros end enums for a better
    handling of the tokens. Also the structs needed for parsing are defined
    here.
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#pragma once

#include "includes.h"

/** \def LOAD
    \brief Integer representation of the LOAD instruction
    This integer representation also correspondes to the machine code
    representation of the LOAD instruction added by 0x100.
*/

/** \def STORE
    \brief Integer representation of the STORE instruction
    This integer representation also correspondes to the machine code
    representation of the STORE instruction added by 0x100.
*/

/** \def ADD
    \brief Integer representation of the ADD instruction
    This integer representation also correspondes to the machine code
    representation of the ADD instruction added by 0x100.
*/

/** \def SUB
    \brief Integer representation of the SUB instruction
    This integer representation also correspondes to the machine code
    representation of the SUB instruction added by 0x100.
*/

/** \def MULT
    \brief Integer representation of the MULT instruction
    This integer representation also correspondes to the machine code
    representation of the MULT instruction added by 0x100.
*/

/** \def QUOT
    \brief Integer representation of the QUOT instruction
    This integer representation also correspondes to the machine code
    representation of the QUOT instruction added by 0x100.
*/

/** \def MODULO
    \brief Integer representation of the MODULO instruction
    This integer representation also correspondes to the machine code
    representation of the MODULO instruction added by 0x100.
*/

/** \def JUMP
    \brief Integer representation of the JUMP instruction
    This integer representation also correspondes to the machine code
    representation of the JUMP instruction added by 0x100.
*/

/** \def JUMPZERO
    \brief Integer representation of the JUMPZERO instruction
    This integer representation also correspondes to the machine code
    representation of the JUMPZERO instruction added by 0x100.
*/

/** \def STOP
    \brief Integer representation of the STOP instruction
    This integer representation also correspondes to the machine code
    representation of the STOP instruction added by 0x100.
*/

#define LOAD 0x158
#define STORE 0x150
#define ADD 0x15A
#define SUB 0x15B
#define MULT 0x15C
#define QUOT 0x15D
#define MODULO 0x15E
#define JUMP 0x147
#define JUMPZERO 0x148
#define STOP 0x145

/** \enum type_of_data
    \brief represents the type of data stored in a list
*/
typedef enum {
  LINE_ELEM,    ///< data is of type line
  LABEL_ELEM,   ///< data is of type label
  VARIABLE_ELEM ///< data is of type variable
} type_of_data;

/** \struct line
    \brief data type in which information about a lines is stored
    All information of the tokens of one line are stored in a variable
    of type line.
*/
typedef struct {
  unsigned int addr; ///< address of the line
  int *lb;           ///< pointer to the label of the line, null if not existent
  int op_code;       ///< integer representation of the operation code
  int *operand; ///< pointer to the operand which can be variable or label and
                ///< which depends on the operation code
} line;

/** \struct label
    \brief data type in which information about a label is stored
    Information about labels are stored in this type of data.
*/
typedef struct {
  unsigned int name; ///< number which is part of the labels name, e.g. "L1" is
                     ///< represented as "1"
  unsigned int addr; ///< destination address of the label
  line *ln;          ///< pointer to the line belonging to this label
} label;

/** \struct variable
    \brief data type in which information about a variable is stored
    Information about variables are stored in tis type of data.
*/
typedef struct {
  char *name;         ///< name of the variable
  unsigned int value; ///< initial value of the variable
  unsigned int addr;  ///< address of the line in which the variable is defined
} variable;

typedef struct listelem listelem;

/** \struct listelem
    \brief data type which is used to create singly linked lists
    This data type contains all information needed to store data in a
    singly linked list.
*/
struct listelem {
  type_of_data tod;      ///< type of data stored in this element
  int *data;             ///< pointer to the data
  struct listelem *next; ///< pointer to te next element in the list
};

/** \brief creates an element containing the data and inserts it at the end of
    the list The data is used to create a new element which is filled with the
    information needed. This new element is inserted at the end of the list.
    @param anchor anchor of the list
    @param tod type of data stored in the list
    @param data pointer to the data which should be inserted into the list
    @return pointer to the new inserted element
*/
extern listelem *insert_elem(listelem **anchor, type_of_data tod, int *data);

/** \brief deletes all elements of a list
    All elements of the list and all the data stored are deleted.
    @param anchor anchor of the list which is to be deleted
*/
extern void deleteall(listelem *anchor);
