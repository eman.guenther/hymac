/** \file parser.h
    \brief contains declarations of global lists, macros and other parser
    related functions
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#pragma once

#include "lexer.h"
#include "parselist.h"

/** \def GENERAL
    \brief integer representation of general error
*/

/** \def MISSING
    \brief integer representation of missing token error
*/

/** \def NOSTOP
    \brief integer representation of missing STOP instruction error
*/

#define GENERAL 265
#define MISSING 266
#define NOSTOP 267

/** \brief anchor variable of list in which all lines are stored
 */
extern listelem *parselist;

/** \brief anchor variable of list in which all labels are stored
 */
extern listelem *labellist;

/** \brief anchor variable of list in which all variables are stored
 */
extern listelem *variablelist;

/** \brief parses all tokens of the code into a parselist
    All tokens of the input code are checked for errors and parsed into
    variables of type line which then are stored in the parselist.
    @param start_addr address of the first line
    @return anchor of the parselist, NULL in case of an error
*/
extern listelem *parse(unsigned int start_addr);

/** \brief cleans up all parser related memory
    The content of parselist, labellist and variablelist are deleted and
    the used memory is freed.
*/
extern void cleanup_parser();
