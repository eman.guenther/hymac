/** \file stack.h
    \brief implementation of a stack
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "includes.h"

/** \brief structure controlling a stack memory storage
    this structure contains the necessary information of a stack which uses
    strings as data
*/
typedef struct {
  int depth;
  char **data;
} Stack;

/** \brief initializes a stack
    initializes a new stack
    @return pointer to the new stack
*/
extern Stack *init_stack();

/** \brief pushes a string to the stack
    @param stack specified stack
    @param data data which should be pushed
*/
extern void push(Stack *stack, char *data);

/** \brief removes the first item of a stack and returns it
    @param stack specified stack
    @return top item of the stack
*/
extern char *pop(Stack *stack);

/** \brief returns the top item of the stack without removing it
    @param stack specified stack
    @return top item of the stack
*/
extern char *top(Stack *stack);

/** \brief deletes a whole stack
    @param stack specified stack
*/
extern void delete_stack(Stack *stack);
