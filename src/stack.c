/** \file stack.c
    \brief implementation of a stack
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "stack.h"

Stack *init_stack() {
  Stack *s = (Stack *)malloc(sizeof(Stack));
  if (s == NULL) {
    printf("error at allocating memory\n");
    return NULL;
  }

  s->depth = 0;
  s->data = NULL;
  push(s, "");
  return s;
}

void push(Stack *stack, char *data) {
  stack->depth++;
  char **new_data = (char **)malloc(sizeof(char *) * stack->depth);
  if (new_data == NULL) {
    printf("error at allocating memory\n");
    return;
  }
  for (int i = 1; i < stack->depth; i++) {
    new_data[i] = stack->data[i - 1];
  }
  new_data[0] = data;
  free(stack->data);
  stack->data = new_data;
}

char *pop(Stack *stack) {
  if (stack->data[0][0] == '\0') {
    return stack->data[0];
  }
  char *d = stack->data[0];
  stack->depth--;
  char **newdata = (char **)malloc(sizeof(char *) * stack->depth);
  if (newdata == NULL) {
    perror(strerror(errno));
    printf("error at malloc\n");
    return NULL;
  }

  for (int i = 0; i < stack->depth; i++) {
    newdata[i] = stack->data[i + 1];
  }

  free(stack->data);
  stack->data = newdata;
  return d;
}

char *top(Stack *stack) { return stack->data[0]; }

void delete_stack(Stack *stack) {
  free(stack->data);
  free(stack);
}
