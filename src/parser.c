/** \file parser.c
    \brief implementation of the parser
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "parser.h"

listelem *parselist;
listelem *labellist;
listelem *variablelist;

/* \brief allocates a variable of type line
     Allocates a variable of type line and initializes it with default values.
     @param addr address of the line
     @return pointer to the new allocated line
*/
line *allocate_line(unsigned int addr) {
  line *new_line =
      (line *)malloc(sizeof(line)); // current line to fill and parse
  if (new_line == NULL) {
    printf("parser: error occured at allocating memory\n");
    return NULL;
  }
  // fill with default values
  new_line->addr = addr;
  new_line->lb = NULL;
  new_line->op_code = 0;
  new_line->operand = NULL;
  return new_line;
}

/* \brief allocates a variable of type label
     Allocates a variable of type label and initializes it with default values.
     @param name name of the label
     @return pointer to the new allocated label
*/
label *allocate_label(unsigned int name) {
  label *lbl = (label *)malloc(sizeof(label));
  if (lbl == NULL) {
    printf("parser: error occured at allocating memory\n");
    return NULL;
  }
  // fill with default values
  lbl->name = name;
  lbl->addr = 0x1000000;
  lbl->ln = NULL;
  return lbl;
}

/* \brief allocates a variable of type variable
     Allocates a variable of type variable and initializes it with default
   values.
     @return pointer to the new allocated variable
*/
variable *allocate_var() {
  variable *new_var =
      (variable *)malloc(sizeof(variable)); // current var to fill and parse
  if (new_var == NULL) {
    printf("parser: error occured at allocating memory\n");
    return NULL;
  }
  // fill with default values
  new_var->name = NULL;
  new_var->value = 0x1000000;
  new_var->addr = 0x1000000;
  return new_var;
}

/* \brief prints the code of the current line
     Prints the code of the currently processed line.
     @param pt pointer to a charakter in the line
*/
void print_code_line(char *pt) {
  char *curr = pt - 1;
  while (*curr != '\n') { // go back to last line break
    curr--;
  }
  curr++;
  printf("\t");
  while (*curr != '\n') { // print one line
    printf("%c", *curr);
    curr++;
  }
  printf("\n");
}

/* \brief prints an error code and message
     @param linenumer number of the line in which the error occured
     @param code the error code
*/
void print_error(int linenumber, int code) {
  char *tokentype;
  printf("parser: error at line %i\n", linenumber);

  switch (code) {
  case OPCODE:
    printf("opcode is not allowed after opcode or operand\n");
    break;
  case ERRONEOUS:
    printf("spelling error: token could not be identified\n");
    break;
  case VARNAME:
    printf("operand cannot occur before opcode\n");
    break;
  case GENERAL:
    if (token.type == EoF)
      tokentype = "EOF";
    else if (token.type == OPCODE)
      tokentype = "opcode";
    else if (token.type == LABEL)
      tokentype = "label";
    else if (token.type == VARNAME)
      tokentype = "varname";
    else if (token.type == NUMBER)
      tokentype = "number";
    else if (token.type == VARNAME)
      tokentype = "varname";
    printf("syntax error: %s cannot appear here\n", tokentype);
    break;
  case END_OF_LINE:
    printf("syntax error: command not complete\n");
    break;
  case NOSTOP:
    printf("syntax error: EOF without STOP command\n");
    break;
  case NUMBER:
    printf("syntax error: number cannot appear after brackets or varname in "
           "vardef part\n");
    break;
  case BRACKET_OPEN:
    printf("syntax error: digit missing before '}'\n");
    break;
  case BRACKET_CLOSE:
    printf("syntax error: digit, '{' or varname missing before '}'\n");
    break;
  }

  print_code_line(token.pt);
}

/** \brief converts a string to an opcode value
    Takes a string from the input and converts it to an opcode value.
    @param pt pointer to the string in the input string
    @param len length of the string
    @return opcode value
*/
int string_to_opcode(char *pt, int len) {
  char *code = (char *)malloc(sizeof(char) * (len + 1));
  if (code == NULL) {
    printf("string_to_opcode: error occured at allocating memory\n");
    return 0;
  }
  for (int i = 0; i < len; i++) {
    code[i] = pt[i];
  }
  code[len] = '\0';

  if (!strcmp(code, "LOAD")) {
    return LOAD;
  } else if (!strcmp(code, "STORE")) {
    return STORE;
  } else if (!strcmp(code, "ADD")) {
    return ADD;
  } else if (!strcmp(code, "SUB")) {
    return SUB;
  } else if (!strcmp(code, "MULT") || !strcmp(code, "MUL")) {
    return MULT;
  } else if (!strcmp(code, "QUOT")) {
    return QUOT;
  } else if (!strcmp(code, "MODULO") || !strcmp(code, "MOD")) {
    return MODULO;
  } else if (!strcmp(code, "JUMP")) {
    return JUMP;
  } else if (!strcmp(code, "JUMPZERO") || !strcmp(code, "JZ")) {
    return JUMPZERO;
  } else if (!strcmp(code, "STOP")) {
    return STOP;
  } else {
    printf("opcode %s was not recognized.\n", code);
    return 0;
  }
  free(code);
}

/** \brief converts a string to an unsigned integer
    @param pt pointer to the string in the input string
    @param len length of the string
    @return converted value
*/
unsigned int string_to_number(char *pt, int len) {
  char *number = (char *)malloc(sizeof(char) * (len + 1));
  if (number == NULL) {
    printf("string_to_number: error occured at allocating memory\n");
    return 0x1000000; // value bigger than 3 byte: cannot occur in hyma
  }
  for (int i = 0; i < len; i++) { // copy number value
    number[i] = pt[i];
  }
  number[len] = '\0';

  unsigned int value =
      (unsigned int)strtoul(number, NULL, 10); // convert to uint
  return value;
}

/** \brief search for a label in the specified label list
    @param anchor anchor of the label list
    @name name of the label
    @return pointer to the label found in the list, otherwise NULL
*/
listelem *search_for_label(listelem *anchor, unsigned int name) {
  listelem *pt = anchor;
  if (pt == NULL)
    return NULL;
  while (pt != NULL) {
    if (((label *)pt->data)->name == name) {
      break;
    }
    pt = pt->next;
  }
  return pt;
}

/** \brief search for a variable in the specified variable list
    @param anchor anchor of the variable list
    @param name of the variable
    @return pointer to the variable found in the list, otherwise NULL
*/
listelem *search_for_varname(listelem *anchor, char *name) {
  listelem *pt = anchor;
  if (pt == NULL)
    return NULL;
  while (pt != NULL) {
    if (!strcmp(((variable *)pt->data)->name, name)) {
      break;
    }
    pt = pt->next;
  }
  return pt;
}

/** \brief checks the specified list for missing information
    Checks a label or variable list for missing information or values.
    @param anchor anchor of the list
    @param tod type of data stored in the list
    @return EXIT_SUCCESS in case of success, EXIT_FAILURE otherwise
*/
int validate_list(listelem *anchor, type_of_data tod) {
  listelem *curr = anchor;
  char *type = NULL;
  if (tod == LABEL_ELEM)
    type = "label";
  if (tod == VARIABLE_ELEM)
    type = "variable";
  if (anchor == NULL) {
    printf("%s list is empty\n", type);
    return EXIT_SUCCESS;
  }
  while (curr != NULL) {
    label *lbl = (label *)curr->data;
    variable *var = (variable *)curr->data;
    switch (tod) {
    case LABEL_ELEM:
      if (lbl->name == 0x1000000) {
        printf("label name is missing\n");
        return EXIT_FAILURE;
      }
      if (lbl->addr == 0x1000000) {
        printf("label address is missing\n");
        return EXIT_FAILURE;
      }
      if (lbl->ln == NULL) {
        printf("label line reference is missing\n");
        return EXIT_FAILURE;
      }
      break;
    case VARIABLE_ELEM:
      if (var->name == NULL) {
        printf("variable name is missing\n");
        return EXIT_FAILURE;
      }
      if (var->value == 0x1000000) {
        printf("variable value is missing\n");
        return EXIT_FAILURE;
      }
      break;
    default:
      printf("some weird stuff\n");
      break;
    }
    curr = curr->next;
  }
  return EXIT_SUCCESS;
}

listelem *parse(unsigned int start_addr) { // returns parselist
  parselist = NULL;
  labellist = NULL;
  variablelist = NULL;

  bool stop_parsed = false; // parsed the STOP instruction?
  unsigned int line_addr = start_addr;
  line *curr_line = allocate_line(line_addr);
  if (curr_line == NULL) {
    return NULL;
  }

  unsigned int lb_name;
  char *tmp_name;

  do {
    get_next_token();
    if (DEBUG) {
      printf("type=%d, line_number=%d, len=%d\n", token.type, token.line_number,
             token.len);
      printf("token=");
      for (int i = 0; i < token.len; i++) {
        printf("%c", token.pt[i]);
      }
      printf("\n");
    }
    switch (token.type) {
    case LABEL:
      lb_name = string_to_number(token.pt + 1, token.len - 1);
      if (lb_name == 0x1000000) {
        return NULL;
      }
      listelem *lbl = search_for_label(labellist, lb_name);
      if (lbl == NULL) { // label not in label list
        label *lb_data = allocate_label(lb_name);
        if (lb_data == NULL) {
          return NULL;
        }
        lbl = insert_elem(&labellist, LABEL_ELEM, (int *)lb_data);
        if (lbl == NULL) {
          printf("parser: error at inserting label\n");
          return NULL;
        }
      }
      if (curr_line->op_code == 0) { // label first token in line
        ((label *)lbl->data)->addr = line_addr;
        ((label *)lbl->data)->ln = curr_line;
        curr_line->lb = lbl->data;
      } else if (curr_line->op_code == JUMP ||
                 curr_line->op_code == JUMPZERO) { // label as operand
        curr_line->operand = lbl->data;
      } else {
        print_error(token.line_number, LABEL);
        return NULL;
      }
      break;
    case OPCODE:
      if (curr_line->op_code != 0 || curr_line->operand != NULL) {
        print_error(token.line_number, OPCODE);
        return NULL;
      }
      curr_line->op_code = string_to_opcode(token.pt, token.len);
      if (curr_line->op_code == 0) {
        return NULL;
      }
      break;
    case VARNAME:
      if (curr_line->op_code == 0) {
        print_error(token.line_number, VARNAME);
        return NULL;
      }

      tmp_name = (char *)malloc(sizeof(char) * (token.len + 1));
      if (tmp_name == NULL) {
        printf("parser: error at allocating memory\n");
        return NULL;
      }
      for (int i = 0; i < token.len; i++) {
        tmp_name[i] = token.pt[i];
      }
      tmp_name[token.len] = '\0';
      listelem *var_entry = search_for_varname(variablelist, tmp_name);
      variable *new_var = NULL;
      if (var_entry == NULL) { // variable not present yet
        new_var = allocate_var();
        if (new_var == NULL)
          return NULL;
        new_var->name = tmp_name;
        new_var->value = 0x1000000;
        var_entry = insert_elem(&variablelist, VARIABLE_ELEM, (int *)new_var);
        if (var_entry == NULL) {
          printf("parser: error occured at inserting variable\n");
          return NULL;
        }
      }
      curr_line->operand = var_entry->data;
      break;
    case EoF:
    case END_OF_LINE:
      if ((curr_line->op_code == 0 || curr_line->operand == NULL) &&
          curr_line->op_code != STOP) {
        print_error(token.line_number, END_OF_LINE);
        return NULL;
      }
      listelem *elem_in_list =
          insert_elem(&parselist, LINE_ELEM, (int *)curr_line);
      if (elem_in_list == NULL) {
        printf("parser: error occured at inserting line\n");
        return NULL;
      }
      if (curr_line->op_code == STOP) {
        stop_parsed = true;
      }
      line_addr += 4;
      curr_line = allocate_line(line_addr);
      if (curr_line == NULL) {
        return NULL;
      }
      break;
    case ERRONEOUS:
      print_error(token.line_number, ERRONEOUS);
      return NULL;
      break;
    default: // everything else, wrong syntax
      print_error(token.line_number, GENERAL);
      return NULL;
      break;
    }
  } while (token.type != EoF &&
           !stop_parsed); // end after parsing EoF token or STOP
  if (!stop_parsed) {     // just EOF without STOP
    print_error(token.line_number, NOSTOP);
    return NULL;
  }

  unsigned int var_value = 0x1000000;
  variable *curr_var = NULL;
  bool bracket_opened = false;
  bool bracket_closed = false;

  do { // just vardefs and EOF
    get_next_token();
    if (DEBUG) {
      printf("type=%d, line_number=%d, len=%d\n", token.type, token.line_number,
             token.len);
      printf("token=");
      for (int i = 0; i < token.len; i++) {
        printf("%c", token.pt[i]);
      }
      printf("\n");
    }
    switch (token.type) {
    case NUMBER:
      if (bracket_opened || bracket_closed || var_value != 0x1000000) {
        print_error(token.line_number, NUMBER);
        return NULL;
      }
      var_value = string_to_number(token.pt, token.len);
      if (var_value == 0x1000000) {
        return NULL;
      }
      break;
    case BRACKET_OPEN:
      if (var_value == 0x1000000) {
        print_error(token.line_number, BRACKET_OPEN);
        return NULL;
      }
      bracket_opened = true;
      break;
    case VARNAME:
      if (!bracket_opened || bracket_closed || var_value == 0x1000000) {
        print_error(token.line_number, VARNAME);
        return NULL;
      }
      char *name = (char *)malloc(sizeof(char) * (token.len + 1));
      if (name == NULL) {
        printf("parser: error at allocating memory\n");
        return NULL;
      }
      for (int i = 0; i < token.len; i++) {
        name[i] = token.pt[i];
      }
      name[token.len] = '\0';

      listelem *var_entry = search_for_varname(variablelist, name);
      if (var_entry == NULL) { // variable not present yet
        curr_var = allocate_var();
        if (curr_var == NULL)
          return NULL;
        curr_var->name = name;
        curr_var->value = var_value;
        var_entry = insert_elem(&variablelist, VARIABLE_ELEM, (int *)curr_var);
        if (var_entry == NULL) {
          printf("parser: error occured at inserting variable\n");
          return NULL;
        }
      } else {
        curr_var = (variable *)var_entry->data;
      }
      curr_var->value = var_value;
      curr_var->addr = line_addr;
      break;
    case BRACKET_CLOSE:
      if (!bracket_opened || curr_var == NULL || curr_var->name == NULL ||
          var_value == 0x1000000) {
        print_error(token.line_number, BRACKET_CLOSE);
        return NULL;
      }
      bracket_closed = true;
      break;
    case EoF:
    case END_OF_LINE:
      if (!bracket_opened && !bracket_closed && curr_var == NULL &&
          var_value == 0x1000000) { // empty line
      } else if (!bracket_opened || !bracket_closed || curr_var->name == NULL ||
                 var_value == 0x1000000) {
        print_error(token.line_number, END_OF_LINE);
        return NULL;
      }
      var_value = 0x1000000;
      curr_var = NULL;
      bracket_opened = false;
      bracket_closed = false;
      line_addr += 4;
      break;
    case ERRONEOUS:
      print_error(token.line_number, ERRONEOUS);
      return NULL;
      break;
    default: // everything else, wrong syntax
      print_error(token.line_number, GENERAL);
      break;
    }
  } while (token.type != EoF); // end after parsing EoF token

  int list_check;
  list_check = validate_list(labellist, LABEL_ELEM);
  if (list_check == EXIT_FAILURE) {
    printf("labellist is not correct. Some information is missing.\n");
    return NULL;
  }
  list_check = validate_list(variablelist, VARIABLE_ELEM);
  if (list_check == EXIT_FAILURE) {
    printf("variablelist is not correct. Some information is missing.\n");
    return NULL;
  }

  return parselist;
}

void cleanup_parser() {
  deleteall(parselist);
  deleteall(labellist);
  deleteall(variablelist);
}
