/** \file code_gen_c.h
    \brief code generation of c code
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#pragma once

#include "controll_flow_graph.h"
#include "includes.h"
#include "parselist.h"
#include "parser.h"
#include "stack.h"

/** \brief generates c code and writes it to a file
    This function generates c code from the controll flow graph and writes it
    into the specified file.
    @param filename the file in which the generated code is written
    @return EXIT_SUCCESS in case of success, EXIT_FAILURE otherwise
*/
extern int generate_c(char *filename);
