/** \file init.h
    \brief Header file of the initialisation of the program.
    In this file all structures and functions needed are declared.
 */
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#pragma once
#include "includes.h"

/** \struct args
    \brief struct for passing the validated arguments back to the driver
    The parameters passed into the program are validated by the function
    check_parameters.
 */
typedef struct {
  int f_opt;          ///< input filename index in argv
  char c_opt;         ///< compile to c ('c'), mc ('m') or both (' ')
  char *o_opt;        ///< output filename index in argv
  unsigned int s_opt; /// hex starting address
} args;

/** \brief prints the usage of the program to stdout
 */
extern void print_usage(void);

/** \brief prints the version of the program to stdout
 */
extern void print_version(void);

/** \brief checks a file for existance and readability
    @param filename The name of the file which should be checked.
    @return EXIT_SUCCESS or EXIT_FAILURE in case of an error
 */
extern int check_file(char *filename);

/** \brief checks the parameters of the program
    All passed parameters are checked for correctness and completeness.
    They are parsed into the args struct.
    @param argc number of parameters
    @param argv list of strings which contains the parameters
    @return struct with parsed parameters
 */
extern args check_parameters(int argc, char **argv);
