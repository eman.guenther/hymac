/** \file includes.h
    \brief A file for all general includes.
    In this file all standard includes are done. Also the debug macro
    for debug output is defined.
 */
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#pragma once

#include <assert.h>
#include <getopt.h>
#include <libgen.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> //just with linux!!

#include <errno.h>

/** \def DEBUG
    \brief A macro for turning debug output on or off.
 */
#define DEBUG 0
