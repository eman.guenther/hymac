/** \file parselist.c
    \brief implementation of a singly linked list
 */
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "parselist.h"

listelem *insert_elem(listelem **anchor, type_of_data tod, int *data) {
  listelem *curr = *anchor;
  listelem *elem =
      (listelem *)malloc(sizeof(listelem)); // allocate memory for new element
  if (elem == NULL) {
    printf("parselist: error at allocating memory\n");
    return NULL;
  }
  elem->tod = tod;
  elem->data = data;

  if (curr != NULL) {            // list is not empty
    while (curr->next != NULL) { // goto last element
      curr = curr->next;
    }
    elem->next = curr->next;
    curr->next = elem; // paste element at last position
  } else {             // list is empty
    elem->next = NULL;
    *anchor = elem;
  }

  return elem;
}

void deleteall(listelem *anchor) {
  if (anchor == NULL)
    return;
  listelem *curr, *help;
  curr = anchor;
  while (curr != NULL) {
    help = curr->next;
    if (curr->tod == VARIABLE_ELEM) {
      free(((variable *)curr->data)->name);
    } else if (curr->tod != LINE_ELEM && curr->tod != LABEL_ELEM) {
      assert(false && "error deleteall: unknown type of data\n");
    }
    free(curr->data);
    free(curr);
    curr = help;
  }
  anchor = NULL;
}
