/** \file lexer.h
    \brief file containing all information for lexer module
    In this file all definitions needed for the lexing module are done.
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#pragma once
#include "includes.h"

/** \def EoF
    \brief end of file token
    Integer representation of the end of file token type.
 */

/** \def OPCODE
    \brief operation code token
    Integer representation of the operation code token type.
 */

/** \def LABEL
    \brief label token
    Integer representation of the label token type.
 */

/** \def VARNAME
    \brief variable name token
    Integer representation of the variable name token type.
 */

/** \def NUMBER
    \brief number token
    Integer representation of the number token type.
 */

/** \def BRACKET_OPEN
    \brief opening bracket token
    Integer representation of the opening bracket token type.
 */

/** \def BRACKET_CLOSE
    \brief closing bracket token
    Integer representation of the closing bracket token type.
 */

/** \def END_OF_LINE
    \brief end of line token
    Integer representation of the end of line token type.
 */

/** \def ERRONEOUS
    \brief erroneous token
    Integer representation of the erroneous token type.
 */

// macro definitions for token types
// numbers > 255 to avoid collisions with ASCII (0..255)
#define EoF 256
#define OPCODE 257
#define LABEL 258
#define VARNAME 259
#define NUMBER 260
#define BRACKET_OPEN 261
#define BRACKET_CLOSE 262
#define END_OF_LINE 263
#define ERRONEOUS 264

/** \struct tk_def
    \brief token structure in which token information is passed
    This structure contains all information needed for parsing and
    handling of the token.
 */
typedef struct {
  int type; ///< integer representation of the type of token
  char *pt; ///< pointer to the token in the string representation of the code
  int len;  ///< length of the token in the code
  int line_number; ///< number of the line in which the token is placed
} tk_def;

/** \brief variable in which the information is passed
    All the information about the token needed is passed into this variable.
    Because it is defined in the header as external variable it can be
    accessed globaly.
 */
extern tk_def token; // token is public for all

/** \brief initialisation of the lexer
    Initialises the lexer which means the code is read from the file and
    some local variables are initialised.
    @param filename the name of the file which contains the code
 */
extern void start_lexer(char *filename);

/** \brief lexes the next token and updates the token variable
    Reads the next token from the code and updates the content of the
    token variable.
 */
extern void get_next_token(void);

/** \brief deallocates all used memory
 */
extern void end_lexer();
