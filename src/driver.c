/** \file driver.c
    \brief driver for the hymac program
    In this file the main function and the main program logic is
    defined.
 */

/** \mainpage hymac Documentation
    The software hymac is a compiler for the hypothetical machine used for the
    course "Grundlagen der Informatik I (I-110) at the HTW Dresden.

    It was created by Emanuel Günther and is mostly licensed under the Apache
    License 2.0.
 */
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "code_gen_c.h"
#include "code_gen_mc.h"
#include "controll_flow_graph.h"
#include "includes.h"
#include "init.h"
#include "lexer.h"
#include "parser.h"

/** \brief cleans up all allocated memory
 */
void cleanup_program();

/** \brief main function of the hymac program
    This function contains the general program logic and calls all
    function needed.
    @param argc number of parameters
    @param argv list of strings containing the parameters
    @return EXIT_SUCCESS or EXIT_FAILURE in case of an error
 */
int main(int argc, char **argv) {
  args a = check_parameters(argc, argv); // get parsed arguments in args struct
  start_lexer(argv[a.f_opt]);            // prepare input

  listelem *parselist = parse(a.s_opt);
  if (parselist == NULL) {
    cleanup_program();
    return EXIT_FAILURE;
  }

  char *ofname;   // output filename
  bool extension; // has ofname already an extension?
  if (a.o_opt != NULL && (a.c_opt == 'c' || a.c_opt == 'm')) {
    ofname = a.o_opt;
    extension = true;
  } else {
    if (a.o_opt == NULL) { // no output file specified
      ofname = strtok(basename(argv[a.f_opt]), ".");
    } else { // output file specified, but two files to be generated
      ofname = strtok(basename(a.o_opt), ".");
    }
    extension = false;
  }

  switch (a.c_opt) {
  case 'c': // generate c code
    if (!extension) {
      if (strcat(ofname, ".c") == NULL) {
        printf("error at strcat\n");
        cleanup_program();
        return EXIT_FAILURE;
      }
    }
    generate_c(ofname);
    break;
  case 'm': // generate mc code
    if (!extension) {
      if (strcat(ofname, ".hmc.txt") == NULL) {
        printf("error at strcat\n");
        cleanup_program();
        return EXIT_FAILURE;
      }
    }
    generate_mc(ofname);
    break;
  default: // generate c and mc code
    if (strcat(ofname, ".c") == NULL) {
      printf("error at strcat\n");
      cleanup_program();
      return EXIT_FAILURE;
    }
    generate_c(ofname);
    ofname = strtok(ofname, ".");
    if (strcat(ofname, ".hmc.txt") == NULL) {
      printf("error at strcat\n");
      cleanup_program();
      return EXIT_FAILURE;
    }
    generate_mc(ofname);
    break;
  }

  cleanup_program();
  return EXIT_SUCCESS;
}

void cleanup_program() {
  end_lexer();
  cleanup_parser();
  delete_graph();
}
