/** \file lexer.c
    \brief implementation of the lexer
 */
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "lexer.h"

// definitions of macro functions for faster calls
#define is_uc_letter(ch) ('A' <= (ch) && (ch) <= 'Z')
#define is_lc_letter(ch) ('a' <= (ch) && (ch) <= 'z')
#define is_digit(ch) ('0' <= (ch) && (ch) <= '9')
#define is_end_of_input(ch) ((ch) == '\0')
#define is_end_of_line(ch) ((ch) == '\n')
#define is_layout(ch) (!is_end_of_input(ch) && (ch) <= ' ')
#define is_bracket(ch) ((ch) == '{' || (ch) == '}')

/* PRIVATE */
static char *input;
static int dot;
static int line_number;

/* PUBLIC */
tk_def token;

// initializes the input and necessary variables
void start_lexer(char *filename) {
  FILE *f;
  int len;

  f = fopen(filename, "rt");
  if (f == NULL) {
    printf("File %s could not be opened. Exiting.\n", filename);
    exit(-1);
  }

  fseek(f, 0, SEEK_END); // seek to the end of the file
  len = ftell(f) + 1;
  fseek(f, 0, SEEK_SET); // seek to the start of the file

  if (len == 0) {
    printf("File is empty. Please specify a valid file. Exiting.\n");
    exit(-1);
  }

  input = (char *)malloc(len); // allocate space for the file content
  if (input == NULL) {
    printf("start_lexer(): malloc failed. Exiting.\n");
    exit(-1);
  }

  if (fread(input, 1, len, f) == -1) {
    printf("Data from file %s could not be read. Exiting.\n", filename);
    exit(-1);
  }

  input[len - 1] = '\0';

  fclose(f);

  dot = 0;
  line_number = 1;
}

void recognize_token(int type) {
  int start = dot;

  dot++;

  if (type != EoF && type != BRACKET_OPEN && type != BRACKET_CLOSE &&
      type != END_OF_LINE) {
    // those are tokens with just one charakter
    while (!is_layout(input[dot]) && !is_bracket(input[dot])) {
      switch (type) { // specific checks for every token type
      case LABEL:
      case NUMBER:
        if (!is_digit(input[dot]))
          type = ERRONEOUS;
        break;
      case VARNAME:
        if (!is_lc_letter(input[dot]))
          type = ERRONEOUS;
        break;
      case OPCODE:
        if (!is_uc_letter(input[dot]))
          type = ERRONEOUS;
        break;
      }
      dot++;
    }
  }

  token.type = type;
  token.pt = &input[start];
  token.len = dot - start;
  token.line_number = line_number;
}

void get_next_token(void) {
  char c; // first input charakter

  // skip layout
  while (is_layout(input[dot]) && !is_end_of_line(input[dot]) &&
         !is_end_of_input(input[dot]))
    dot++;

  c = input[dot];

  if (is_digit(c)) { // [0..9]
    recognize_token(NUMBER);
  } else if (is_lc_letter(c)) { // [a..z]
    recognize_token(VARNAME);
  } else if (is_uc_letter(c)) {
    if (c == 'L') {
      if (is_digit(input[dot + 1])) { // L[0..9]
        recognize_token(LABEL);
      } else if (is_uc_letter(input[dot + 1])) { // L[A..Z]
        recognize_token(OPCODE);
      } else { // L + other input charakter
        recognize_token(ERRONEOUS);
      }
    } else { // [A..Z]
      recognize_token(OPCODE);
    }
  } else if (c == '{') {
    recognize_token(BRACKET_OPEN);
  } else if (c == '}') {
    recognize_token(BRACKET_CLOSE);
  } else if (c == '/') {
    if (input[dot + 1] == '/') { // "//"
      while (!is_end_of_line(input[dot]) && !is_end_of_input(input[dot]))
        dot++;
    } else { // "/" + other input charakter
      recognize_token(ERRONEOUS);
    }
  } else if (is_end_of_line(c)) { // '\n'
    recognize_token(END_OF_LINE);
    line_number++;
  } else if (is_end_of_input(c)) { // '\0'
    recognize_token(EoF);
  } else { // other input charakters
    recognize_token(ERRONEOUS);
  }
}

void end_lexer() {
  if (input != NULL) {
    free(input);
    input == NULL;
  }
}
