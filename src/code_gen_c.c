/** \file code_gen_c.c
    \brief implementation of the c code generation
*/
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "code_gen_c.h"

/** \def PC_SEQ
    \brief process code sequence mode
    Macro for the code processing. All code will be generated.
*/
/** \def PC_COD
    \brief process code code mode
    Macro for the code processing. Code without JUMP|JUMPZERO will be generated.
*/
/** \def PC_CON
    \brief process code condition mode
    Macro for the code processing. Just conditions (JUMP&&JUMPZERO) will be
   generated.
*/
/** \def PC_LOOP
    \brief process code loop mode
    Macro for the code processing. Just negated conditions will be generated.
*/

#define PC_SEQ 0
#define PC_COD 1
#define PC_CON 2
#define PC_LOOP 3

FILE *output = NULL;
int nbytes = 0;

/** \brief generates source code which outputs the values of all variables
 */
void print_variable_state() {
  listelem *vars = variablelist;
  variable *var = NULL;

  nbytes = fprintf(output, "\n");
  if (nbytes < 0) {
    fclose(output);
    return;
  }

  while (vars != NULL) {
    var = (variable *)vars->data;

    nbytes =
        fprintf(output, "\tprintf(\"%s = %%d\", %s);\n", var->name, var->name);
    if (nbytes < 0) {
      fclose(output);
      return;
    }

    vars = vars->next;
  }
}

/** \brief takes one expression from the stack
    Removes one expression from the stack and generates a code representation of
    it.
    @param stack current stack
    @param buffer buffer for the code output
    @param bufferIndex current index of the buffer
*/
void examine_stack(Stack *stack, char buffer[64], int *bufferIndex) {
  int len;
  char *item = pop(stack);
  while (true) {
    if (item[0] == '+' || item[0] == '-' || item[0] == '*' || item[0] == '/' ||
        item[0] == '%') {
      len = strlen(top(stack));
      strncpy(buffer + (*bufferIndex - len), top(stack), len);
      *bufferIndex -= len;
      buffer[--(*bufferIndex)] = item[0];
      pop(stack);
    } else if (item[0] == '\0') {
      strncpy(buffer + (*bufferIndex) - 3, "acc", 3);
      *bufferIndex -= 3;
      break;
    } else {
      len = strlen(item);
      strncpy(buffer + (*bufferIndex) - len, item, len);
      *bufferIndex -= len;
      break;
    }
    item = pop(stack);
  }
}

/** \brief saves all remaining operations on the stack to the accumulator
    @param stack current stack
    @param buffer buffer for code output
    @param bufferIndex current index of the buffer
*/
void save_to_accumulator(Stack *stack, char buffer[64], int *bufferIndex) {
  examine_stack(stack, buffer, bufferIndex);
  nbytes = fprintf(output, "\tacc = %s;\n", buffer + *bufferIndex);
  if (nbytes < 0) {
    fclose(output);
    return;
  }

  for (int i = *bufferIndex; i < 63; i++) { // clear buffer
    buffer[i] = '\0';
  }
  *bufferIndex = 63;
}

/** \brief generates the code representation of a block
    Takes the source code of a node and creates a C representation of it.
    @param node node which code should be generated
    @param mode specifies the generation mode
*/
void process_code(bblock *node, int mode) {
  listelem *curr_line = node->ln;
  line *ln;
  Stack *stack = init_stack();
  char buffer[64];
  buffer[63] = '\0';
  int bufferIndex = 63;

  for (int i = 0; i < node->numLines; i++, curr_line = curr_line->next) {
    ln = (line *)curr_line->data;
    switch (ln->op_code) {
    case LOAD:
      if (mode == PC_CON || mode == PC_LOOP)
        continue;
      push(stack, ((variable *)ln->operand)->name);
      break;
    case STORE:
      if (mode == PC_CON || mode == PC_LOOP)
        continue;
      examine_stack(stack, buffer, &bufferIndex);
      nbytes = fprintf(output, "\t%s = %s;\n", ((variable *)ln->operand)->name,
                       buffer + bufferIndex);
      if (nbytes < 0) {
        fclose(output);
        return;
      }
      push(stack, ((variable *)ln->operand)->name);
      for (int i = bufferIndex; i < 63; i++) { // clear buffer
        buffer[i] = '\0';
      }
      bufferIndex = 63;
      break;
    case ADD:
      if (mode == PC_CON || mode == PC_LOOP)
        continue;
      push(stack, ((variable *)ln->operand)->name);
      push(stack, "+");
      break;
    case SUB:
      if (mode == PC_CON || mode == PC_LOOP)
        continue;
      push(stack, ((variable *)ln->operand)->name);
      push(stack, "-");
      break;
    case MULT:
      if (mode == PC_CON || mode == PC_LOOP)
        continue;
      push(stack, ((variable *)ln->operand)->name);
      push(stack, "*");
      break;
    case QUOT:
      if (mode == PC_CON || mode == PC_LOOP)
        continue;
      push(stack, ((variable *)ln->operand)->name);
      push(stack, "/");
      break;
    case MODULO:
      if (mode == PC_CON || mode == PC_LOOP)
        continue;
      push(stack, ((variable *)ln->operand)->name);
      push(stack, "%");
      break;
    case JUMP:
      break;
    case JUMPZERO:
      if (mode == PC_COD)
        continue;
      examine_stack(stack, buffer, &bufferIndex);
      if (mode == PC_LOOP) {
        nbytes = fprintf(output, "%s != 0", buffer + bufferIndex);
      } else {
        nbytes = fprintf(output, "%s == 0", buffer + bufferIndex);
      }
      if (nbytes < 0) {
        fclose(output);
        return;
      }
      for (int i = bufferIndex; i < 63; i++) { // clear buffer
        buffer[i] = '\0';
      }
      bufferIndex = 63;

      break;
    case STOP:
      break;
    }
  }

  if (strcmp(top(stack), "")) { // if stack not empty
    save_to_accumulator(stack, buffer, &bufferIndex);
  }

  delete_stack(stack);
}

/** \brief analyzes a node by recognizing structures
    The function recursively analyzes nodes. Multiple cases are detected
    and generated. The function calls itself again with the following
    nodes.
    @param node node which will be analyzed
    @param eoc node which is a recognized end of a condition. Will not
    be used if not processing the end of condition case.
    @return In general the analyzed node. If a condition is recognized
    it returns the end-of-condition node. In case of an error NULL is
    returned.
*/
bblock *analyze_node(bblock *node,
                     bblock *eoc) { // analyze one node in the graph
  bblock *next_node = node->numOutEdges ? node->outEdges[1] : NULL;
  if (!node->marked) { // block not marked
    node->marked = true;
    // don't forget about the linear links at *Edges[0] !!

    if ((node->numInEdges > 2 || node->numInEdges == 1) &&
        node->numOutEdges == 3) {
      // check for loop or condition with multiple branches
      bool isLoop = false;
      for (int i = 1; i < node->numInEdges; i++) {
        if (!isLoop && node->inEdges[i]->marked)
          isLoop = true;
        else if (isLoop && node->inEdges[i]->marked)
          isLoop = false;
      }
      if (node->numInEdges == 1) {
        isLoop = true;
      }
      if (isLoop) { // loop
        if (node->inEdges[node->numInEdges == 1 ? 0 : 2]->numLines >
            1) { // while
          process_code(node, PC_COD);
          nbytes = fprintf(output, "\twhile (");
          if (nbytes < 0) {
            fclose(output);
            return NULL;
          }
          process_code(node, PC_LOOP);
          nbytes = fprintf(output, ") {\n");
          if (nbytes < 0) {
            fclose(output);
            return NULL;
          }
          process_code(node, PC_COD);
          if (analyze_node(node->outEdges[2], NULL) == NULL) {
            return NULL;
          }
          process_code(node, PC_COD);
          nbytes = fprintf(output, "\t}\n");
          if (nbytes < 0) {
            fclose(output);
            return NULL;
          }
        } else { // do-while
          nbytes = fprintf(output, "\tdo {\n");
          if (nbytes < 0) {
            fclose(output);
            return NULL;
          }
          process_code(node, PC_COD);
          if (analyze_node(node->outEdges[2], NULL) == NULL) {
            return NULL;
          }
          nbytes = fprintf(output, "\t} while (");
          if (nbytes < 0) {
            fclose(output);
            return NULL;
          }
          process_code(node->inEdges[node->numInEdges == 1 ? 0 : 2]->inEdges[1],
                       PC_LOOP);
          nbytes = fprintf(output, ");\n");
          if (nbytes < 0) {
            fclose(output);
            return NULL;
          }
        }
      } else { // end of condition and new condition
        nbytes = fprintf(output, "\t}\n");
        if (nbytes < 0) {
          fclose(output);
          return NULL;
        }
        node->marked = false;
        next_node = node;
      }
    } else if (node->numOutEdges == 3) { // condition
      process_code(node, PC_COD);
      nbytes = fprintf(output, "\tif (");
      if (nbytes < 0) {
        fclose(output);
        return NULL;
      }
      process_code(node, PC_CON);
      nbytes = fprintf(output, ") {\n");
      if (nbytes < 0) {
        fclose(output);
        return NULL;
      }

      bblock *eoc_ret = analyze_node(node->outEdges[1], eoc);
      if (next_node == NULL) {
        return NULL;
      }
      nbytes = fprintf(output, "\telse\n\t{\n");
      if (nbytes < 0) {
        fclose(output);
        return NULL;
      }
      next_node = analyze_node(node->outEdges[2], eoc_ret);
      if (next_node == NULL) {
        return NULL;
      }
    } else if (node->numInEdges > 2) { // end of condition
      if (next_node == NULL || node == eoc) {
        nbytes = fprintf(output, "\t}\n");
        if (nbytes < 0) {
          fclose(output);
          return NULL;
        }
        return node;
      } else { // if node != eoc && next_node != NULL
        process_code(node, PC_SEQ);
      }
    } else if (node->numInEdges == 2 || node->numInEdges == 0) { // seqence
      process_code(node, PC_SEQ);
    } else { // not detected case
      nbytes = fprintf(output, "\t// not detected case\n");
      if (nbytes < 0) {
        fclose(output);
        return NULL;
      }
    }
  } else { // block marked
    if ((node->numInEdges > 2 || node->numInEdges == 1) &&
        node->numOutEdges == 3) {
      bool isEndOfLoop = false;
      for (int i = 1; i < node->numOutEdges; i++) {
        if (node->outEdges[i]->marked)
          isEndOfLoop = true;
      }
      if (isEndOfLoop) { // end of loop
        return node;
      } else { // end of condition with new condition
        nbytes = fprintf(output, "\t}\n");
        if (nbytes < 0) {
          fclose(output);
          return NULL;
        }
        node->marked = false;
        next_node = node;
      }
    } else if (node->numInEdges > 2) { // end of condition
      if (node == eoc) {               // if real end of condition
        nbytes = fprintf(output, "\t}\n");
        if (nbytes < 0) {
          fclose(output);
          return NULL;
        }
        return node;
      } else {
        process_code(node, PC_SEQ);
      }
    } else if (node->numInEdges == 2) { // sequence
      process_code(node, PC_SEQ);
    } else { // not detected case
      nbytes = fprintf(output, "\t// not detected case\n");
      if (nbytes < 0) {
        fclose(output);
        return NULL;
      }
    }
  } // end of 'is marked' condition

  if (next_node == NULL) {
    return node;
  } else {
    return analyze_node(next_node, eoc);
  }
}

/** \brief main function for generating c code from the parselist
    The function will call a function to generate the controll flow
    graph. After that the c source code is generated by analyzing all
    nodes. All output is written to a file.
    @param filename name of the file in which the output is written
    @return EXIT_SUCCESS in case of success, EXIT_FAILURE otherwise
*/
int generate_c(char *filename) {
  bblock *graph = create_cfg(parselist);
  if (graph == NULL) {
    return EXIT_FAILURE;
  }

  output = fopen(filename, "w");
  if (output == NULL) {
    printf("generate_c: error at opening the output file\n");
    return EXIT_FAILURE;
  }

  // generate header
  nbytes = fprintf(output, "void main() {\n");
  if (nbytes < 0) {
    fclose(output);
    return EXIT_FAILURE;
  }

  nbytes = fprintf(output, "\tunsigned int acc = 0; // accumulator\n");
  if (nbytes < 0) {
    fclose(output);
    return EXIT_FAILURE;
  }

  listelem *vars = variablelist;
  variable *var = NULL;
  while (vars != NULL) {
    var = (variable *)vars->data;

    nbytes =
        fprintf(output, "\tunsigned int %s = %d;\n", var->name, var->value);
    if (nbytes < 0) {
      fclose(output);
      return EXIT_FAILURE;
    }

    vars = vars->next;
  }

  nbytes = fprintf(output, "\n");
  if (nbytes < 0) {
    fclose(output);
    return EXIT_FAILURE;
  }

  // generate c code and write it to file (or stdout for debugging)
  bblock *rbb = analyze_node(graph, NULL);
  if (rbb == NULL) {
    fclose(output);
    return EXIT_FAILURE;
  }

  // end values of all variables should be printed out from the generated
  // program
  print_variable_state();

  // generate footer
  nbytes = fprintf(output, "}\n");
  if (nbytes < 0) {
    fclose(output);
    return EXIT_FAILURE;
  }

  printf("output generated: %s\n", filename);

  delete_graph();
  fclose(output);
  return EXIT_SUCCESS;
}
