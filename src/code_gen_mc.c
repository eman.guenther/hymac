/** \file code_gen_mc.c
    \brief implementation of the machine code code generation
 */
/*
hymac - compiler for a hypothetical machine language
Copyright 2019 Emanuel Günther <eman.guenther@gmx.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "code_gen_mc.h"

char *generate_mc(char *filename) {
  FILE *fp = fopen(filename, "w"); // open output file
  if (fp == NULL) {
    printf("generate_mc: error at opening the output file\n");
    return NULL;
  }
  listelem *pl = parselist;
  listelem *vl = variablelist;
  int nbytes = 0;

  while (pl != NULL) { // go through all lines
    line *ln = (line *)pl->data;
    nbytes = fprintf(fp, "%2x", ln->op_code & 0x000000FF);
    if (nbytes < 0) {
      printf("generate_mc: error at writing to file\n");
      fclose(fp);
      return NULL;
    }

    if (ln->op_code == JUMP || ln->op_code == JUMPZERO) { // jumping operations
      unsigned int jmp_addr = ((line *)((label *)ln->operand)->ln)->addr;
      nbytes = fprintf(fp, "%06x\n", jmp_addr);
      if (nbytes < 0) {
        printf("generate_mc: error at writing to file\n");
        fclose(fp);
        return NULL;
      }
    } else if (ln->op_code == STOP) { // stop instruction
      nbytes = fprintf(fp, "000000\n");
      if (nbytes < 0) {
        printf("generate_mc: error at writing to file\n");
        fclose(fp);
        return NULL;
      }
    } else { // calculating operations
      unsigned int var_addr = ((variable *)ln->operand)->addr;
      nbytes = fprintf(fp, "%06x\n", var_addr);
      if (nbytes < 0) {
        printf("generate_mc: error at writing to file\n");
        fclose(fp);
        return NULL;
      }
    }

    pl = pl->next;
  }

  // loop for variables
  while (vl != NULL) {
    variable *var = (variable *)vl->data;
    nbytes = fprintf(fp, "00%06x\n", var->value);
    if (nbytes < 0) {
      printf("generate_mc: error at writing to file\n");
      fclose(fp);
      return NULL;
    }
    vl = vl->next;
  }

  printf("output generated: %s\n", filename);

  fclose(fp);
  return filename;
}
